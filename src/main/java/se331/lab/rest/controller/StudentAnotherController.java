package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.mapper.MapperUtilImpl;
import se331.lab.rest.service.StudentAnotherService;
@Controller
@Slf4j
public class StudentAnotherController {
    @Autowired
    StudentAnotherService studentAnotherService;
    @GetMapping("/student/name/{name}")
    public ResponseEntity<?> getStudentByNameContains(@PathVariable String name){
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(this.studentAnotherService.getStudentByNameContains(name)));
                //studentAnotherService.getStudentByNameContains(name));
    }

    @GetMapping("/student/advisor/{name}")
    public ResponseEntity<?> getStudentByAdvisorName(@PathVariable String name){
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentAnotherService.getStudentWhoseAdvisorNameIs(name)));
    }

}
